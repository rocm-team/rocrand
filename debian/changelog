rocrand (5.7.1-6) unstable; urgency=medium

  * autopkgtest: Switch test runner over to official rocm-test-launcher
    rocm-test-launcher is now provided by pkg-rocm-tools.

 -- Christian Kastner <ckk@debian.org>  Tue, 25 Feb 2025 09:57:48 +0100

rocrand (5.7.1-5) unstable; urgency=medium

  * autopkgtest: Convert to the use of rocm-testhelper
    rocm-testhelper is embedded for evaluation purposes; it will be made
    available separately soon.
  * autopkgtest: Save GTest results as artifact

 -- Christian Kastner <ckk@debian.org>  Wed, 16 Oct 2024 19:15:47 +0200

rocrand (5.7.1-4) unstable; urgency=medium

  * d/rules: use CMAKE_SKIP_INSTALL_RPATH to enable rpath in build tests

 -- Cordell Bloor <cgmb@slerp.xyz>  Sun, 07 Apr 2024 20:59:16 -0600

rocrand (5.7.1-3) unstable; urgency=medium

  * Add d/p/0005-rocrand-public-hip-host.patch to fix roc::rocrand CMake
    target dependencies.
  * Add d/p/0006-hiprand-public-hip-host.patch to fix hip::hiprand CMake
    target dependencies.

 -- Cordell Bloor <cgmb@slerp.xyz>  Sun, 31 Mar 2024 23:40:09 -0600

rocrand (5.7.1-2) unstable; urgency=medium

  * Migrate to unstable

 -- Cordell Bloor <cgmb@slerp.xyz>  Fri, 08 Mar 2024 16:07:58 -0700

rocrand (5.7.1-2~exp1) experimental; urgency=medium

  * Update Build-Depends to require clang-17
  * Enable gfx1100, gfx1101 and gfx1102 architectures
  * Drop patchelf and rocminfo from Build-Depends

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 28 Feb 2024 16:33:09 -0700

rocrand (5.7.1-1) unstable; urgency=medium

  * Migrate to unstable
  * New upstream version 5.7.1

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 17 Jan 2024 23:56:35 -0700

rocrand (5.7.0-1~exp2) experimental; urgency=medium

  [ Cordell Bloor ]
  * Add librocrand-doc and libhiprand-doc packages providing
    HTML documentation.
  * Update upstream URLs

  [ Christian Kastner ]
  * autopkgtests: Export dmesg and other info as artifacts

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 10 Jan 2024 14:36:40 -0700

rocrand (5.7.0-1~exp1) experimental; urgency=medium

  * New upstream version 5.7.0
  * Drop patches included upstream
    - 0001-hide-kernel-symbols.patch
    - 0002-remove-git-dependency.patch
  * dbgsym: Disable dwz and switch to compressed DWARF-5

 -- Christian Kastner <ckk@debian.org>  Thu, 26 Oct 2023 20:02:36 +0200

rocrand (5.5.1-2) unstable; urgency=medium

  * Filter cf-protection hardening from device code.
    Fixes a FTBFS with dpkg >= 1.22

 -- Christian Kastner <ckk@debian.org>  Thu, 14 Sep 2023 14:12:34 +0200

rocrand (5.5.1-1) unstable; urgency=medium

  [ Cordell Bloor ]
  * New upstream version 5.5.1
  * Drop git from Build-Depends
  * Hide library symbols that are not intended to be
    part of the public ABI.
  * d/rules: drop gfx1011 architecture from librocrand1.
    The HIP runtime will fall back to using gfx1010.
  * d/control: fix nocheck dependencies

  [ Christian Kastner ]
  * Add support for the 'nocheck' build profile

 -- Cordell Bloor <cgmb@slerp.xyz>  Sat, 29 Jul 2023 01:32:56 -0600

rocrand (5.3.3-6) unstable; urgency=medium

  * d/rules: explicitly disable lto
  * d/rules: drop unnecessary CFLAGS filtering
  * d/t/control: limit autopkgtest architectures

 -- Cordell Bloor <cgmb@slerp.xyz>  Tue, 04 Jul 2023 23:57:48 -0600

rocrand (5.3.3-5) unstable; urgency=medium

  * Migrate to unstable.

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 14 Jun 2023 13:15:07 -0600

rocrand (5.3.3-5~exp1) experimental; urgency=medium

  * Reintroduce the changes we temporarily reverted for bookworm, these being:
    - Add myself to Uploaders
    - Fix Maintainer name
    - Add packages librocrand1-tests, libhiprand1-tests providing autopkgtests
      In previous versions, these were called -test (singular). However, those
      packages never left unstable, and were reverted for bookworm
    - Reduce arch to amd64, arm64, ppc64el
    - d/rules: enable hardening flags
    - d/rules: enable gfx1010 and gfx1011
  * libhiprand-dev depends on librocrand-dev
    This seems to have been overlooked when fixing #1035787

 -- Christian Kastner <ckk@debian.org>  Sun, 11 Jun 2023 13:50:43 +0200

rocrand (5.3.3-4) unstable; urgency=medium

  * Temporarily revert fixes unfit for bookworm.
    Specifically, revert all changes from after 5.3.3-1.

  * Add missing dependency on libamdhip64-dev (Closes: #1035784, #1035787)

 -- Christian Kastner <ckk@debian.org>  Sun, 28 May 2023 18:25:03 +0200

rocrand (5.3.3-3) unstable; urgency=medium

  * Upload to unstable.

 -- Christian Kastner <ckk@debian.org>  Sun, 16 Apr 2023 22:45:08 +0200

rocrand (5.3.3-3~exp1) experimental; urgency=medium

  * Add myself to Uploaders
  * Fix Maintainer name
  * Add packages librocrand1-test, libhiprand1-test providing autopkgtests

 -- Christian Kastner <ckk@debian.org>  Thu, 13 Apr 2023 23:41:30 +0200

rocrand (5.3.3-2) unstable; urgency=medium

  [ Christian Kastner ]
  * Reduce arch to amd64, arm64, ppc64el

  [ Cordell Bloor ]
  * d/rules: enable hardening flags
  * d/rules: enable gfx1010 and gfx1011

 -- Cordell Bloor <cgmb@slerp.xyz>  Mon, 06 Mar 2023 00:41:11 -0700

rocrand (5.3.3-1) unstable; urgency=medium

  * d/{watch,gbp.conf}: recombine with hiprand as MUT
  * New upstream version 5.3.3
  * d/{control,rules,*.install}: update install dirs
    Closes: #1028957
  * d/control: update standards version
  * d/lib{roc,hip}rand-dev.examples: remove examples
  * d/not-installed: ignore test binaries
  * d/librocrand1.symbols.amd64: add new symbols
  * add d/patches/0001-hide-kernel-symbols.patch
  * d/librocrand1.symbols.amd64: update symbols
  * d/copyright: update copyright file
  * d/rules: fix debug symbols

 -- Cordell Bloor <cgmb@slerp.xyz>  Tue, 07 Feb 2023 00:06:45 -0700

rocrand (5.0.0-2) unstable; urgency=medium

  [ Étienne Mollier ]
  * d/rules: append reminder for fhs compliance.

  [ Cordell Bloor ]
  * d/control: add missing dependency on rocm-cmake
  * d/rules: remove unnecessary options and variables

  [ Étienne Mollier ]
  * d/control: stick to ROCm short form in packages descriptions.

 -- Étienne Mollier <emollier@debian.org>  Wed, 16 Nov 2022 21:37:21 +0100

rocrand (5.0.0-1) unstable; urgency=medium

  [ Maxime Chambonnet ]
  * Initial release. (Closes: #1019227)

 -- Étienne Mollier <emollier@debian.org>  Wed, 14 Sep 2022 22:05:49 +0200
