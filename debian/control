Source: rocrand
Section: devel
Homepage: https://github.com/ROCm/rocRAND
Priority: optional
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rocm-team/rocrand.git
Vcs-Browser: https://salsa.debian.org/rocm-team/rocrand
Maintainer: Debian ROCm Team <debian-ai@lists.debian.org>
Uploaders: Maxime Chambonnet <maxzor@maxzor.eu>,
           Étienne Mollier <emollier@debian.org>,
           Cordell Bloor <cgmb@slerp.xyz>,
           Christian Kastner <ckk@debian.org>,
Build-Depends: debhelper-compat (= 13),
               cmake,
               hipcc (>= 5.6.1~),
               libamd-comgr-dev (>= 6.0~),
               libamdhip64-dev (>= 5.6.1~),
               libhsa-runtime-dev (>= 5.7.1~),
               rocm-cmake,
               libgtest-dev <!nocheck>,
Build-Depends-Indep: dh-sequence-sphinxdoc <!nodoc>,
               doxygen <!nodoc>,
               python3-breathe <!nodoc>,
               python3-exhale <!nodoc>,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               libjs-jquery <!nodoc>,
               libjs-mathjax <!nodoc>,
               libjs-sphinxdoc <!nodoc>,
               libjs-underscore <!nodoc>
Rules-Requires-Root: no

Package: librocrand1
Section: libs
Architecture: amd64 arm64 ppc64el
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: generate pseudo- and quasi-random numbers - library
 The rocRAND project provides functions that generate pseudo-random and
 quasi-random numbers.
 .
 The rocRAND library is implemented in the HIP programming language and
 optimised for AMD's latest discrete GPUs. It is designed to run on top of
 AMD's ROCm runtime, but it also works on CUDA enabled GPUs.
 .
 This package provides the AMD ROCm rocRAND library.

Package: librocrand-dev
Section: libdevel
Architecture: amd64 arm64 ppc64el
Depends: librocrand1 (= ${binary:Version}),
         libamdhip64-dev,
         ${misc:Depends},
         ${shlibs:Depends},
Suggests: librocrand-doc
Description: generate pseudo- and quasi-random numbers - headers
 The rocRAND project provides functions that generate pseudo-random and
 quasi-random numbers.
 .
 The rocRAND library is implemented in the HIP programming language and
 optimised for AMD's latest discrete GPUs. It is designed to run on top of
 AMD's ROCm runtime, but it also works on CUDA enabled GPUs.
 .
 This package provides AMD ROCm rocRAND development headers.

Package: librocrand1-tests
Section: libdevel
Architecture: amd64 arm64 ppc64el
Build-Profiles: <!nocheck>
Depends: librocrand1 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
Description: generate pseudo- and quasi-random numbers - test binaries
 The rocRAND project provides functions that generate pseudo-random and
 quasi-random numbers.
 .
 The rocRAND library is implemented in the HIP programming language and
 optimised for AMD's latest discrete GPUs. It is designed to run on top of
 AMD's ROCm runtime, but it also works on CUDA enabled GPUs.
 .
 This package provides AMD ROCm rocRAND test binaries. These binaries are
 only used to check whether rocRAND works correctly on a given GPU, so
 generally speaking, users probably don't need this package.

Package: librocrand-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax,
Description: generate pseudo- and quasi-random numbers - documentation
 The rocRAND project provides functions that generate pseudo-random and
 quasi-random numbers.
 .
 The rocRAND library is implemented in the HIP programming language and
 optimised for AMD's latest discrete GPUs. It is designed to run on top of
 AMD's ROCm runtime, but it also works on CUDA enabled GPUs.
 .
 This package provides the AMD ROCm rocRAND documentation.

Package: libhiprand1
Section: libs
Architecture: amd64 arm64 ppc64el
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: wrapper library to port from cuRAND applications to HIP - library
 The rocRAND project includes a wrapper library called hipRAND which allows
 user to easily port CUDA applications that use cuRAND library to the HIP
 layer. In ROCm environment hipRAND uses rocRAND, however in CUDA environment
 cuRAND is used instead.
 .
 This package provides AMD ROCm hipRAND library.

Package: libhiprand-dev
Section: libdevel
Architecture: amd64 arm64 ppc64el
Depends: libhiprand1 (= ${binary:Version}),
         libamdhip64-dev,
         librocrand-dev,
         ${misc:Depends},
         ${shlibs:Depends},
Suggests: libhiprand-doc
Description: wrapper library to port from cuRAND applications to HIP - headers
 The rocRAND project includes a wrapper library called hipRAND which allows
 user to easily port CUDA applications that use cuRAND library to the HIP
 layer. In ROCm environment hipRAND uses rocRAND, however in CUDA environment
 cuRAND is used instead.
 .
 This package provides AMD ROCm hipRAND development headers.

Package: libhiprand1-tests
Section: libdevel
Architecture: amd64 arm64 ppc64el
Build-Profiles: <!nocheck>
Depends: libhiprand1 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
Description: wrapper library to port from cuRAND applications to HIP - tests
 The rocRAND project includes a wrapper library called hipRAND which allows
 user to easily port CUDA applications that use cuRAND library to the HIP
 layer. In ROCm environment hipRAND uses rocRAND, however in CUDA environment
 cuRAND is used instead.
 .
 This package provides AMD ROCm hipRAND test binaries. These binaries are
 only used to check whether hipRAND works correctly on a given GPU, so
 generally speaking, users probably don't need this package.

Package: libhiprand-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
Description: wrapper library to port from cuRAND applications to HIP - documentation
 The rocRAND project includes a wrapper library called hipRAND which allows
 user to easily port CUDA applications that use cuRAND library to the HIP
 layer. In ROCm environment hipRAND uses rocRAND, however in CUDA environment
 cuRAND is used instead.
 .
 This package provides the AMD ROCm hipRAND documentation.
